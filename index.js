const express = require('express')
const app = express()
const port = 3000
app.use(express.json()) 
app.use(express.urlencoded({extended: true}))


//A GET method request and response from an ExpressJS API
app.get('/home', (req, res) => {
	res.send('Welcome to the home page')
})


//A GET method request and response from an ExpressJS API to retrieve data.
let users = [
	{
		"username": "thecure89",
		"password": "robertsmith1"
	}

]

app.get('/users', (req,res) => {
	res.send(users)
})

//A DELETE method request and response from an ExpressJS API to delete a resource.


app.delete('/delete-user', (req,res) => {
	res.send(`${req.body.username} has been deleted`)

})

app.listen(port, () => console.log(`Server is running at port ${port}`))
